export const getExchangeBase = (base = "IDR", money = 0) => {
    return new Promise((resolve, reject) => {

        fetch(`https://api.exchangeratesapi.io/latest?base=${base}`)
            .then(res => res.json())
            .then((res) => {
                let result = [];
                Object.keys(res.rates).forEach((key) => {
                    result.push({
                        base: key,
                        buy: parseFloat((res.rates[key] * money) * (102/100)).toFixed(4),
                        rate: parseFloat((res.rates[key] * money)).toFixed(4),
                        sell: parseFloat((res.rates[key] * money) * (98/100)).toFixed(4),
                    });
                });
                resolve(result);
            })
            .catch(err => reject(err));
    });

}
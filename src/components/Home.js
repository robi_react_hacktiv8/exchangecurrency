import React, { Component } from 'react';
import { getExchangeBase } from '../api/ExchangeRate';
import '../styles/App.css';

export default class Home extends Component {

    state = {
        data: [],
        filterCollection: ['CAD', 'IDR', 'JPY', 'CHF', 'EUR', 'USD']
    }

    componentDidMount() {
        getExchangeBase('IDR', 15123.0567)
            .then(res => this.setState({ data: res }))
            .catch(err => console.log(err));
    }

    render() {
        let filteredData = this.state.data.filter(item => this.state.filterCollection.includes(item.base));
        return (
            <div>
                <center>
                    <table>
                        <thead>
                            <tr>
                                <th></th>
                                <th>
                                    WE BUY
                                </th>
                                <th>
                                    EXCHANGE RATE
                                </th>
                                <th>
                                    WE SELL
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                filteredData.length <= 0 ? null : filteredData.map(item => {
                                    return (
                                        <tr>
                                            <th><center>{item.base}</center></th>
                                            <td><center>{item.buy}</center></td>
                                            <td><center>{item.rate}</center></td>
                                            <td><center>{item.sell}</center></td>
                                        </tr>
                                    )
                                })
                            }
                        </tbody>
                    </table>
                    <p>* base currency is IDR<br />* As for the API,&nbsp;<a href="https://exchangeratesapi.io/">https://exchangeratesapi.io/</a>&nbsp;is used.</p>
                </center>
            </div>
        )
    }
}